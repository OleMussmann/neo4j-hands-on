# Neo4j hands-on

## Launch the sandbox
1. Go to the [Neo4j Sandbox](https://sandbox.neo4j.com). Log in with your google account or create a new one.
2. Click on `Movies` and `Launch Project`. Wait for Sandbox to start. Click `Open with Browser`.
3. You will see a few tutorial screens. On each of them, first click on the code to copy it into the console and then run the code by clicking on the triangle ▶️ on the right next to the copied code. Also explore the additional questions at the bottom of each tutorial page!
4. After exploring the output, click `Next >` to go to the next screen.

> :warning: Some capitalization in the sandbox tutorial is wrong. Make sure to always capitalize node types like 'Person' or 'Movie'!

## Answers
1. Which three movies were published before 1990?
2. Summon a graph that shows which actors played in which movies that were released between 2008 and 2009. Which two actors played in two movies?
3. Create a 'Person' node in the graph with your name as 'name' property. What is your query?
4. Create a 'Movie' node with the 'title' as "The CBS Action Movie". What is your query?
5. Create a relationship 'ACTED_IN' between you and the movie you created. RETURN p, m, w. Show it as a graph (left side of the result box). What is your query?
6. Create another relationship, let 'Tom Hanks' also act in 'The CBS Action Movie'. What is your query?
7. Have you ever heard of 'Jan de Bont'? No? Find out how you are related to him! Run a shortest path query between you and him, limit it to 15 hops (for performance reasons). Replace NUMBER_OF_HOPS and both NAME_X and run the query below. How many hops separate you? What's the path?


```
MATCH (start:Person),(end:Person),
p = shortestPath((start)-[*..NUMBER_OF_HOPS]-(end)) 
WHERE start.name = "NAME_1" AND end.name = "NAME_2"
RETURN p
```